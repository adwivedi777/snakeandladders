package  
{
		import flash.events.TimerEvent;
		import flash.display.movieClip;
		import flash.utils.Timer;
		import flash.events.Event;
		import flash.display.MovieClip;
		import asunit.textui.TestRunner;
	import flash.display.Sprite;
	import AllTests;
	import flash.events.Event;
	import Board;

	/**
	 * ...
	 * @author ashu
	 */
	public class game extends MovieClip;
	{
		
		public function game() 
		{



stage.tilesCoordinates = [];
stage.tileSize=100;// size (length and height)of each tile in Px;




function rollDice(){
var rand= Math.ceil(Math.random()*6);
return rand;
}
//tells the character to move X tiles


stage.numberPlayers = 2; // the number of players.
stage.playersArray=[2];
//array to contain all the player characters
//call this to create a player 
function newCharacter(){
var char = new CharacterMC();
char.currentTile= 1;

//tileNo char is on
char.numberOfTilesToMove=0;
//Number of tiles the char needs to move. Set using moveXTiles
//the currentTile of the character
//characterMc = class name of your character
//makes the char move a square every timer tick, if  its numberOfTilesToMove >0
//this function also return a value true or false depending on whether it is
//still has tiles to move or not
char.move=function ():Boolean{
if(this.numberOfTilesToMove>0){
this.currentTile++;
this.numberOfTilesToMove--;
}
if (stage.ladders[this.currentTile]!=null){
//a ladder or snake is present on the tile you move on
//teleport to another tile
this.currentTile = stage.ladders[this.currentTile]
this.currentTile =stage.snakes[this.currentTile]
}
//tells the player character to move a number of tiles,determine by diceRoll 
char.moveXTiles=function(tiles:uint){
this.numberOfTilesToMove=tiles;
}
//updates the position of the movieClip 
this.render();
if(this.numberOfTilesToMove>0){
return true;
}else{
return false;
}
}
for (var i=1;i<=stage.numberPlayers;i++){
//creates each of the players pieces
newCharacter();
}
char.render=function (){
var t= this.currentTile;
//since the positions of the center of tile #X are stored in tileCoordinates
//retrieve the coordinates and set the character's coordinates to them
this.x= stage.tileCoordinates[t];
this.y= stage.tileCoordinates[t];
}

char.render();
//puts the character piece on the 1st square
stage.playersArray.push(char);
}
//this function moves the characters
stage.allowTurn = true;
//if allow turn is true, the next player can roll the dice, but if its false, you have to wait until the current player's piece finishes moving
stage.moveCharacters = function(evt:Event){
var anyCharStillMoving= false;

//are still moving
for(var i=0;i<stage.playersArray.length;i++){
var playerChar= stage.playersArray[i];
//tell the char to move, if it has numberOfTilesToMove>0
var stillMoving =playerChar.move();
if (stillMoving ==true){
anyCharStillMoving = true;
}
stage.allowTurn =anyCharStillMoving;
}
}

stage.timer = new Timer(interval);
//interval if the time between tile movements. For example, if interval = 1000//ms, the character will move to a next tile every 1000ms if its //numberOfTilesToMove >0
stage.timer.addEventListener(TimerEvent.TIMER,stage.moveCharacters);

stage.currentTurnNo = 0;
//its the 1st players turn when the game starts. 1st player is stored 
//in index 0 of playersArray
//Create a Button and name it RollDice. When it is pressed, the dice is rolled and the player's turn is passed to the next player.
function clickHandler(evt:Event){
if(stage.allowTurn == true){
//only allow clicks when a character peice is not moving
//executes whenever RollDice is clicked
var currentPlayer= stage.playersArray(stage.currentTurnNo);
//the current player's turn
var diceResult = rollDice();
//get a dice result
currentPlayer.moveXTiles(diceResult);
stage.currentTurnNo++;
if(stage.currentTurnNo>=stage.playersArray.length){
// its the last player, loop back to the 1st
stage.currentTurnNo = 0;
}
}
}

RollDice.addEventListener(Event.CLICK, clickHandler);
nextFrame();
			
		}
		
	}

}
