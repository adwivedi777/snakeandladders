package 
{
	import asunit.textui.TestRunner;
	import flash.display.Sprite;
	import AllTests;
	import flash.events.Event;
	import Board;
	import asunit.textui.TestRunner;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author ashu
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			
			//to call the alltest constructor...
			
			var unittests:TestRunner = new TestRunner();
			stage.addChild(unittests);
			unittests.start(AllTests , null , TestRunner.SHOW_TRACE);
			
			removeEventListener(Event.ADDED_TO_STAGE, init);
		 	// entry point
		}
		
	}
	
}